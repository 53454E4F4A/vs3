
package koordinatorServiceClient;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "KoordinatorServiceService", targetNamespace = "http://koordinatorService/", wsdlLocation = "http://0.0.0.0:9998/hawmetering/Koordinator?wsdl")
public class KoordinatorServiceService
    extends Service
{

    private final static URL KOORDINATORSERVICESERVICE_WSDL_LOCATION;
    private final static WebServiceException KOORDINATORSERVICESERVICE_EXCEPTION;
    private final static QName KOORDINATORSERVICESERVICE_QNAME = new QName("http://koordinatorService/", "KoordinatorServiceService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://0.0.0.0:9998/hawmetering/Koordinator?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        KOORDINATORSERVICESERVICE_WSDL_LOCATION = url;
        KOORDINATORSERVICESERVICE_EXCEPTION = e;
    }

    public KoordinatorServiceService() {
        super(__getWsdlLocation(), KOORDINATORSERVICESERVICE_QNAME);
    }

    public KoordinatorServiceService(WebServiceFeature... features) {
        super(__getWsdlLocation(), KOORDINATORSERVICESERVICE_QNAME, features);
    }

    public KoordinatorServiceService(URL wsdlLocation) {
        super(wsdlLocation, KOORDINATORSERVICESERVICE_QNAME);
    }

    public KoordinatorServiceService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, KOORDINATORSERVICESERVICE_QNAME, features);
    }

    public KoordinatorServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public KoordinatorServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns KoordinatorService
     */
    @WebEndpoint(name = "KoordinatorServicePort")
    public KoordinatorService getKoordinatorServicePort() {
        return super.getPort(new QName("http://koordinatorService/", "KoordinatorServicePort"), KoordinatorService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns KoordinatorService
     */
    @WebEndpoint(name = "KoordinatorServicePort")
    public KoordinatorService getKoordinatorServicePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://koordinatorService/", "KoordinatorServicePort"), KoordinatorService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (KOORDINATORSERVICESERVICE_EXCEPTION!= null) {
            throw KOORDINATORSERVICESERVICE_EXCEPTION;
        }
        return KOORDINATORSERVICESERVICE_WSDL_LOCATION;
    }

}
