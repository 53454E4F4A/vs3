
package koordinatorServiceClient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the koordinatorServiceClient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SensorExists_QNAME = new QName("http://koordinatorService/", "SensorExists");
    private final static QName _MeterInUseException_QNAME = new QName("http://koordinatorService/", "MeterInUseException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: koordinatorServiceClient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MeterInUseException }
     * 
     */
    public MeterInUseException createMeterInUseException() {
        return new MeterInUseException();
    }

    /**
     * Create an instance of {@link SensorExistsException }
     * 
     */
    public SensorExistsException createSensorExistsException() {
        return new SensorExistsException();
    }

    /**
     * Create an instance of {@link WebSensor }
     * 
     */
    public WebSensor createWebSensor() {
        return new WebSensor();
    }

    /**
     * Create an instance of {@link WebMeter }
     * 
     */
    public WebMeter createWebMeter() {
        return new WebMeter();
    }

    /**
     * Create an instance of {@link WebMeterArray }
     * 
     */
    public WebMeterArray createWebMeterArray() {
        return new WebMeterArray();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SensorExistsException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://koordinatorService/", name = "SensorExists")
    public JAXBElement<SensorExistsException> createSensorExists(SensorExistsException value) {
        return new JAXBElement<SensorExistsException>(_SensorExists_QNAME, SensorExistsException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeterInUseException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://koordinatorService/", name = "MeterInUseException")
    public JAXBElement<MeterInUseException> createMeterInUseException(MeterInUseException value) {
        return new JAXBElement<MeterInUseException>(_MeterInUseException_QNAME, MeterInUseException.class, null, value);
    }

}
