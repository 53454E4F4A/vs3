package main;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import koordinatorService.KoordinatorService;
import koordinatorServiceClient.MeterInUseException_Exception;
import sensorService.SensorService;
import sensorService.WebKoordinator;
import sensorService.WebSensor;
import sensorServiceClient.WebSensorArray;


public class Main {

	public static String createMeterUrl(String ip, String port, String CardDir) {
		return "http://" + ip + ":" + port +"/hawmetering/" + CardDir;
		
	}
	private static String createSensorUrl(String otherSensorIp,
			String otherSensorPort) {
		// TODO Auto-generated method stub
		return "http://"+ otherSensorIp +":" + otherSensorPort + "/hawmetering/SensorService";
	}
	
	public static void main(String[] args) throws IOException {
		int i;
		String listenPort = null;
		String listenIp = null;
		String meterArgs[];
		String koordinatorPort = null;
		List<String> meterUrls = new LinkedList<String>();
		SensorService s;
		
		String otherSensorPort = null;
		String otherSensorIp = null;
		
		try {			
		
			// arg 0 kein port -> ip von anderem sensor
			otherSensorIp = args[0];
			otherSensorPort = args[1];
			koordinatorPort = args[2];
			listenIp = args[3];
			listenPort = args[4];
			meterArgs = Arrays.copyOfRange(args, 5, args.length);
		
		} catch(ArrayIndexOutOfBoundsException e){
			
			System.out.println("usage: java Sensor.main.Main <IP> <PORT> (of other Sensor)  <PORT> (of koordinator) <IP> <PORT> (to listen) <IP> <PORT> <DIR>... (triplets of webmeter url port and direction)" );
			return;	
		}
		
	
		for(i = 0; i < meterArgs.length; i=i+3) {
			meterUrls.add(createMeterUrl(meterArgs[i], meterArgs[i+1], meterArgs[i+2]));
		}
		
	

		 s = new SensorService("SensorService", "hawmetering/", listenIp, listenPort, koordinatorPort,									
				meterUrls.toArray(new String[meterUrls.size()]));
		s.askForKoordinator(createSensorUrl(otherSensorIp, otherSensorPort));
		
		System.out.println("Registering " + meterUrls.size() + " meters");		
		for(String sd : meterUrls) System.out.println(sd);
		
		try {
			s.register();
		} catch (MeterInUseException_Exception e) {
			System.out.println("meters in use");
			s.shutdown();
			return;
		}
	

		return;
		
	}



}
