
package sensorServiceClient;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the sensorServiceClient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: sensorServiceClient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WebSensorArray }
     * 
     */
    public WebSensorArray createWebSensorArray() {
        return new WebSensorArray();
    }

    /**
     * Create an instance of {@link WebSensor }
     * 
     */
    public WebSensor createWebSensor() {
        return new WebSensor();
    }

    /**
     * Create an instance of {@link WebKoordinator }
     * 
     */
    public WebKoordinator createWebKoordinator() {
        return new WebKoordinator();
    }

}
