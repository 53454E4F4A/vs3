package debag;

public class Logger {
	static final boolean debagging = true;
	static long time = System.currentTimeMillis();
	public static void log(String tag, String format, Object ... msg) {
		if(debagging)
		System.out.println("[" + (System.currentTimeMillis()-time) + "] " + tag + ": " + String.format(format, msg)); 
	
	}
	
	
}
