package koordinatorService;

public class MeterInUseException extends Exception {

	public MeterInUseException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2007950319906971507L;

}
