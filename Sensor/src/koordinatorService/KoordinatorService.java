package koordinatorService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;


import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import debag.Logger;
import sensorService.WebMeter;
import sensorService.WebSensor;
import sensorServiceClient.WebSensorArray;

@WebService
@SOAPBinding(style = Style.RPC)
public class KoordinatorService {
	Endpoint ep;
	public boolean stopped = false;
	private MesswertGenerator mwg;
	private String name;
	private String port;
	private String publish_url;
	private HashMap<WebSensor, Set<WebMeter>> registeredSensors = null;

	public KoordinatorService(String name, String ip, String port) {
		ep = Endpoint.create(this);
		mwg = new MesswertGenerator(this);
		registeredSensors = new HashMap<WebSensor, Set<WebMeter>>();		
		this.setName(name);
		this.setPort(port);
		publish_url = "http://"+ip+":" + port + "/hawmetering/" + name;
		ep.publish(publish_url);
		
		Logger.log("Koordinator", "published " + publish_url);
		mwg.start();
	}

	public void unregisterSensor(@WebParam(name = "sensor") WebSensor sensor) {
		
		registeredSensors.remove(sensor);
	}
	public void stopKoordinatorService() {
		stopped = true;
		ep.stop();
		
	
	}
	public void registerSensor(@WebParam(name = "sensor") WebSensor sensor,
			@WebParam(name = "meters") WebMeter meters[])
			throws MeterInUseException, SensorExistsException {

		// Check for Sensor name
		Logger.log("Koordinator", "Registration from " + sensor.client_url);
		if (registeredSensors.containsKey(sensor)) {
			Logger.log("Koordinator", "already exists, throwing exception");
			throw new SensorExistsException("Sensor existiert bereits");
		}

		// Check for meters
		Set<WebMeter> requested_wm_set = new HashSet<WebMeter>(
				Arrays.asList(meters));
		
		for (Entry<WebSensor, Set<WebMeter>> wm_set : registeredSensors
				.entrySet()) {
			//
			System.out.println(wm_set.getKey() + " has " );
			for(WebMeter m : wm_set.getValue()) {
				System.out.println(m.client_url);
			}
			
			Set<WebMeter> intersect = new HashSet<WebMeter>(requested_wm_set);
			intersect.retainAll(wm_set.getValue());
			if (intersect.size() > 0) {
				Logger.log("Koordinator", "meters in use, throwing exception");
				throw new MeterInUseException(
						"Meter in use by Sensor with url: "
								+ wm_set.getKey().client_url);
			} else {
				System.out.println("keine meters in use");
			}

		}
		Logger.log("Koordinator", "sensor registered successfully");
		// Add registered sensor
		
		registeredSensors.put(sensor, requested_wm_set);
		
	}

	@WebMethod(exclude = true)
	public void signalSensors(int messwert) {
		WebSensorArray a = new WebSensorArray();
		for(WebSensor ws :  registeredSensors.keySet()) {
			a.getItem().add(ws.marshallForSensor());
		}
		
		Logger.log("Koordinator", "signaling sensors");
		for(WebSensor ws :  registeredSensors.keySet()) {	
			//System.out.println("calling updateSensor");
			//a.getItem().addAll(new HashSet<WebSensor>(this.registeredSensors.keySet()));
			try{
			ws.getInstance().updateSensor(messwert, a);
			} catch (Exception e) {
				registeredSensors.remove(ws);
			}
		}
	}

	@WebMethod(exclude = true)
	public String getPublishUrl() {
		return publish_url;
	}
	@WebMethod(exclude = true)
	public String getPort() {
		return port;
	}
	@WebMethod(exclude = true)
	public void setPort(String port) {
		this.port = port;
	}
	@WebMethod(exclude = true)
	public String getName() {
		return name;
	}
	@WebMethod(exclude = true)
	public void setName(String name) {
		this.name = name;
	}

}