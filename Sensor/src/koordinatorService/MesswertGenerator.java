package koordinatorService;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import debag.Logger;

public class MesswertGenerator {
		KoordinatorService koordinator;
	
		MesswertGenerator(KoordinatorService k) {
			koordinator = k;
		}

        final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

        final Runnable ticker = new Runnable() {
            @Override
            public void run() {
                long lTicks = new Date().getTime();
                int messwert = ((int) (lTicks % 20000)) / 100;
                if (messwert > 100) {
                    messwert = 200 - messwert;
                }
                Logger.log("MesswertGenerator", "Messwert: %6d Ticks: %10d", messwert, lTicks);
                koordinator.signalSensors(messwert);
              
            }
        };
        //Starten:
        final ScheduledFuture<?> tickerHandle =
            scheduler.scheduleAtFixedRate(ticker, 2000 - new Date().getTime() % 2000, 2000, TimeUnit.MILLISECONDS);
        
        public void start() {
        //Stoppen nach 20 sec einplanen:
        scheduler.schedule(new Runnable() {
            @Override
            public void run() {
            	
                tickerHandle.cancel(true);
                scheduler.shutdown();
                koordinator.stopKoordinatorService();
            }
        }, 200, TimeUnit.SECONDS);
        }

		public void stop() {
			// TODO Auto-generated method stub
			
		}
    
}
