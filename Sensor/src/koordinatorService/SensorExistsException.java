package koordinatorService;

import javax.xml.ws.WebFault;

@WebFault(name = "SensorExists")
public class SensorExistsException extends Exception {
	public SensorExistsException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2875147860386931096L;
}
