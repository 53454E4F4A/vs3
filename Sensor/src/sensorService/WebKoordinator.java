package sensorService;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class WebKoordinator {
	public String client_url;
	static private final String namespace = "http://koordinatorService/";
	static private final String name = "KoordinatorServiceService";
	koordinatorServiceClient.KoordinatorService koordinator = null;
	public WebKoordinator(String url) {
		this.client_url = url;

	}
	WebKoordinator() {
		
	}

	public koordinatorServiceClient.KoordinatorService getInstance() {
		if( koordinator == null) 
		try {
			Service service = koordinatorServiceClient.KoordinatorServiceService
					.create(new URL(client_url), new QName(
							WebKoordinator.namespace, WebKoordinator.name));
			 koordinator = service
					.getPort(koordinatorServiceClient.KoordinatorService.class);
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return koordinator;
	}
	
	
	public sensorServiceClient.WebKoordinator marshallForSensor() {
		sensorServiceClient.WebKoordinator s = new sensorServiceClient.WebKoordinator();
		s.setClientUrl(this.client_url);
		return s;
	}
	
}
