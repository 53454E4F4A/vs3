package sensorService;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class WebSensor {
	public String client_url;
	public static final String namespace = "http://sensorService/";
	public static final String name = "SensorServiceService";
	private sensorServiceClient.SensorService sensorService = null;
	public WebSensor() {
		
	}
	public WebSensor(String clientUrl) {
		this.client_url = clientUrl;
	}

	public sensorServiceClient.SensorService getInstance() {
		if(sensorService == null)
		try {
			Service service = sensorServiceClient.SensorServiceService.create(
					new URL(client_url), new QName(
							WebSensor.namespace, WebSensor.name));
			//System.out.println(service);
		
			sensorService = service
					.getPort(sensorServiceClient.SensorService.class);
			return sensorService;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return sensorService;
	}
	@Override
	public int hashCode() {
		return this.client_url.hashCode();
	}
	@Override
	public boolean equals(Object other) {
		if(other instanceof WebSensor) return this.client_url.equals(((WebSensor) other).client_url);
		return false;
	}

	public koordinatorServiceClient.WebSensor marshallForKoordinator() {
		koordinatorServiceClient.WebSensor s = new koordinatorServiceClient.WebSensor();
		s.setClientUrl(this.client_url);
		return s;
	}
	public sensorServiceClient.WebSensor marshallForSensor() {
		sensorServiceClient.WebSensor s = new sensorServiceClient.WebSensor();
		s.setClientUrl(this.client_url);
		return s;
	}
	
	@Override
	public
	String toString() {
		return this.client_url;
	}
	
	public 
	int myCompareTo(WebSensor other) {
		return other.client_url.compareTo(this.client_url);
		
	}
	public sensorServiceClient.SensorService getNInstance() {
		
		try {
			Service service = sensorServiceClient.SensorServiceService.create(
					new URL(client_url), new QName(
							WebSensor.namespace, WebSensor.name));
			//System.out.println(service);
		
			sensorService = service
					.getPort(sensorServiceClient.SensorService.class);
			return sensorService;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return sensorService;
	}

	

}
