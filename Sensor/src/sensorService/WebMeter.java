package sensorService;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import debag.Logger;

public class WebMeter {
	public String client_url;
	static private final String namespace = "http://hawmetering/";
	static private final String name = "HAWMeteringWebserviceService";
	HAWMeterClient.HAWMeteringWebservice meter = null;

	WebMeter(String ip, String port, String path, String name) {
		this.client_url = "http://" + ip +":"+ port + "/" + path + name + "?wsdl";
	}
	WebMeter(String client_url) {
		this.client_url = client_url;
	}
	WebMeter() {
		
	}
	public HAWMeterClient.HAWMeteringWebservice getInstance(
		
			) {
		if(meter == null)
		try {
			Service service = HAWMeterClient.HAWMeteringWebserviceService
					.create(new URL(client_url), new QName(
							WebMeter.namespace, WebMeter.name));
			meter = service
					.getPort(HAWMeterClient.HAWMeteringWebservice.class);
		
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			Logger.log("WebMeter", "MalformedURL");
		} catch ( javax.xml.ws.WebServiceException e) {
			Logger.log("WebMeter", "No Connection");
		}
		return meter;
	}


	@Override
	public boolean equals(Object other) {
		if(other instanceof WebMeter) return this.client_url.equals(((WebMeter) other).client_url);
		return false;
	}
	@Override
	public int hashCode() {
		return this.client_url.hashCode();
	}

	public koordinatorServiceClient.WebMeter marshallMeter() {
		koordinatorServiceClient.WebMeter meter = new koordinatorServiceClient.WebMeter();
		meter.setClientUrl(this.client_url);
		return meter;
	}

}
