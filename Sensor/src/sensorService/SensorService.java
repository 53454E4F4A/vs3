package sensorService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import HAWMeterClient.HAWMeteringWebservice;
import debag.Logger;
import koordinatorService.KoordinatorService;
import koordinatorService.MeterInUseException;
import koordinatorService.SensorExistsException;
import koordinatorServiceClient.MeterInUseException_Exception;
import koordinatorServiceClient.SensorExistsException_Exception;
import koordinatorServiceClient.WebMeterArray;


@WebService
@SOAPBinding(style = Style.RPC)

public class SensorService {
	private KoordinatorService koordinatorService;
	private WebSensor myWebSensor;
	private Set<sensorService.WebSensor> all_sensors;
	private List<WebMeter> this_meters;
	private WebKoordinator koordinator = null;	

	private String name;
	private String port;
	private String publish_url;
	final Semaphore timeoutSema = new Semaphore(1, true);
	private Boolean inElection = false;
	String koordinatorPort;
	private Thread timeoutThread;
	 Runnable timeoutRunnable = new TimeoutRunnable();
	private Endpoint ep;
	private String ip;
	 class TimeoutRunnable implements Runnable {
			 boolean running = true;
         @Override
         public void run() {
        	while(running) {
             try {
				if(!timeoutSema.tryAcquire(10,  TimeUnit.SECONDS)) {
						 Logger.log("TimeoutThread", "Koordinator timed out, starting election");
						 running = false;
				    	 startElection();
				    	
				 }
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	}
        	 
             
         }
     };
     
	public SensorService(String name, String path, String ip, String port, String koordinatorPort, String ... meterUrls) {
		this.setName(name);
		this.setPort(port);	
		this.ip = ip;
		this.koordinatorPort = koordinatorPort;
		publish_url = "http://"+ ip +":" + port + "/" + path + name;
		this.myWebSensor = new WebSensor(publish_url);
		this.this_meters = new LinkedList<WebMeter>();
        this.all_sensors = Collections.newSetFromMap(new ConcurrentHashMap<WebSensor, Boolean>());;
        this.koordinator = new WebKoordinator("U");
        for(String s : meterUrls) {
        	this.this_meters.add(new WebMeter(s));
        }
		ep = Endpoint.create(this);
		ep.publish(publish_url);
		Logger.log("Sensor", "published " + publish_url);
        
	}

    public void register() throws MeterInUseException_Exception {
    	Logger.log("Sensor", "trying to register at " + koordinator.client_url);
		WebMeterArray toRegister = 	new WebMeterArray();
		for(WebMeter wm : this_meters) {
			toRegister.getItem().add(wm.marshallMeter());
		}
		
		try {
			koordinator.getInstance().registerSensor(myWebSensor.marshallForKoordinator(), toRegister) ;
			timeoutRunnable = new TimeoutRunnable();
			this.timeoutThread = new Thread(timeoutRunnable);
			timeoutThread.start();
		} catch ( SensorExistsException_Exception e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
			System.out.println("sesnor exists is in use");
		} 
	}
    
    public void askForKoordinator(String publishUrl) {
    	WebSensor s = new WebSensor(publishUrl);
    	sensorServiceClient.SensorService client = s.getInstance();
    	if(client != null) {
	    	sensorServiceClient.WebKoordinator k = client.getKoordinator();
	    	this.koordinator = new WebKoordinator(k.getClientUrl());
	    	
    	}  else {
    		KoordinatorService k = new KoordinatorService("Koordinator", this.ip, this.koordinatorPort);
    		this.koordinator = new WebKoordinator(k.getPublishUrl());
    	}
    	
    }

 
    public WebKoordinator getKoordinator() {	 
    
    	return koordinator;
  }   
 
  	public void updateSensor(@WebParam(name = "messWert") int messWert,
  							 @WebParam(name = "allSensors") WebSensor allSensors[]) {
  	  
  	  this.all_sensors.addAll(Arrays.asList(allSensors));
  	
  	  Logger.log("Sensor", "updating meters");
  	  timeoutSema.release();
	  for(WebMeter wm : this.this_meters) {
		  HAWMeteringWebservice client = wm.getInstance();
		  if(client != null) {
			  client.setTitle("DANGER");
			  client.setValue(messWert);
		  }
	  }
  	}
  	public void startElection() {
  		boolean iAmKoordinator = true;
  		WebSensor sBefore = null;
  		
  		//if(!inElection) inElection = true;
  		//else return;
  		WebSensor[] sensors = this.all_sensors.toArray(new WebSensor[this.all_sensors.size()]);
  		Arrays.sort(sensors, new Comparator<WebSensor>() {		

			@Override
			public int compare(WebSensor o1, WebSensor o2) {
				// TODO Auto-generated method stub
				return o1.client_url.compareTo(o2.client_url);
			}
  			
  		});
  		
  		for(int i = 0; i < sensors.length; i++) {
  			if(sensors[i].client_url.compareTo(this.publish_url) != 0)  			
  			if(sensors[i].getInstance() != null)  {
  				System.out.println(sensors[i].client_url + " is alive");
  				try{
  				sensors[i].getInstance().getKoordinator();
  				iAmKoordinator = false;
  				} catch (Exception e) {
  					this.all_sensors.remove(sensors[i]);
  					
  				}
  			} else {
  				
  				System.out.println("removed " + sensors[i].client_url);
  				this.all_sensors.remove(sensors[i]);
  			}
  		}

  		
  		
  		sensors = this.all_sensors.toArray((new WebSensor[all_sensors.size()]));
  		if(sensors[0].client_url.compareTo(this.publish_url) == 0) {
  			iAmKoordinator = true;
  		
  		}
  		System.out.println(sensors[0].client_url);
  		/*Logger.log("Election", "sensor count %d",  all_sensors.size());
  		for(WebSensor s : all_sensors) {
  			if(sBefore != null && sBefore.equals(s)) 
  				Logger.log("Election", s.client_url);
  			sBefore = s;	
  		}
  		for(WebSensor s : this.all_sensors) {
  			if(!s.equals(this) && this.myWebSensor.myCompareTo(s) < 0) {
  				try {
  					sensorServiceClient.SensorService WebServicePort = s.getInstance();
  					if(WebServicePort != null) WebServicePort.startElection();
  					else {
  						System.out.println("service down!!!");
  					}
  					// ab hier gibt es keine exception also connection alive
  					// ich bin also kein koordinator
  					Logger.log("Election",  this.publish_url + " ist kein koordinator, " + s.client_url + " alive ");
  					iAmKoordinator = false;
  					break;
  					
  				
  				} catch (Exception e) {
  					this.all_sensors.remove(s);
  					Logger.log("Election",  s.client_url + " timed out ");
  					// TODO Auto-generated catch block
  					//e.printStackTrace();
  				} 
  				
  			} 
  		}
  		
  		*/
  		if(iAmKoordinator) { 
  			
  		
  		   
  		
	  			if(koordinatorService == null || koordinatorService.stopped) {
	  				koordinatorService = new KoordinatorService("Koordinator", ip, koordinatorPort);
	  				Logger.log("Election",  "Neuer Koordinator " + this.publish_url);
	  	  			
	  	  			this.koordinator = new WebKoordinator(koordinatorService.getPublishUrl());
	  	  			
	  	  			for(WebSensor s : this.all_sensors) {
	  	  			sensorServiceClient.SensorService WebServicePort = s.getInstance();
	  	  				if(WebServicePort != null) WebServicePort.setKoordinator(this.koordinator.marshallForSensor());
	  	  			}
	  	  				
	  			}
  			
  			
  			
  			
  		}
  	}
  	public void setKoordinator(WebKoordinator k) {
  		this.koordinator = k;
  		inElection = false;
  		try {
  		register();
  		} catch( MeterInUseException_Exception e) {
  		
  			System.out.println("mter in use");
  		}
  	}
  
	@WebMethod(exclude = true)	
	public String getPublishUrl() {
		return publish_url;
	}
	@WebMethod(exclude = true)
	public String getName() {
		return name;
	}
	@WebMethod(exclude = true)
	public void setName(String name) {
		this.name = name;
	}
	@WebMethod(exclude = true)
	public String getPort() {
		return port;
	}
	@WebMethod(exclude = true)
	public void setPort(String port) {
		this.port = port;
	}
	@WebMethod(exclude = true)
	public WebSensor getWebSensor() {
		return this.myWebSensor;
	}
	@WebMethod(exclude = true)
	public void shutdown() {
		this.ep.stop();
	}
  
}